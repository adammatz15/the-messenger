const linkResolver = require('./src/utils/linkResolver')

module.exports = {
	siteMetadata: {
		title: 'The Messenger',
		description: '',
	},
	plugins: [{
			resolve: 'gatsby-source-prismic',
			options: {
				repositoryName: 'the-messenger',
				linkResolver: () => (doc) => linkResolver(doc),
				schemas: {
					homepage: require('./custom_types/homepage.json'),
					navigation: require('./custom_types/navigation.json'),
					page: require('./custom_types/page.json'),
				},
			},
		},
		'gatsby-plugin-react-helmet',
		'gatsby-transformer-sharp',
		'gatsby-plugin-sharp',
		'gatsby-plugin-sass',
		{
			resolve: 'gatsby-plugin-manifest',
			options: {
				icon: 'src/images/favicon.png',
			},
		},
		{
			resolve: 'gatsby-source-filesystem',
			options: {
				name: 'images',
				path: `${__dirname}/src/images`,
			},
		},
		{
			resolve: 'gatsby-plugin-google-fonts',
			options: {
				fonts: [`Lato\:400,400,700,700i,900`, `Amiri\:400,400,700,700i`],
			},
		},
	],
}