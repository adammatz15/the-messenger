import React from "react";
import { Link, graphql } from "gatsby";
import { RichText } from "prismic-reactjs";

const Header = ({ isHomepage, navigation }) => {
	if (!navigation) return null;
	const homepageClass = isHomepage ? "homepage-header" : "";
	const topNav = navigation.data.top_navigation;
	const nav = navigation.data;

	return (
		<header className={`site-header ${homepageClass}`}>
			<head>
				<link
					rel="stylesheet"
					href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
					integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l"
					crossorigin="anonymous"
				></link>
				<link rel="preconnect" href="https://fonts.googleapis.com" />
				<link
					rel="preconnect"
					href="https://fonts.gstatic.com"
					crossorigin
				/>
				<link
					href="https://fonts.googleapis.com/css2?family=Oswald:wght@400;600&display=swap"
					rel="stylesheet"
				/>
			</head>

			<nav className="navbar navbar-light navbar-expand-lg py-3 px-4">
				<Link to="/">
					<div className="logo">
						<img src={nav.nav_logo.url} alt="" className="logo" />
					</div>
				</Link>
				<div className="collapse navbar-collapse" id="navbarCollapse">
					<ul className="navbar-nav ml-auto text-right">
						{topNav.map((navItem, index) => {
							return (
								<li
									className="nav-item"
									data-toggle="collapse"
									data-target=".navbar-collapse.show"
									key={`link-${index}`}
								>
									<Link
										className="nav-link"
										to={navItem.link.url}
									>
										{RichText.asText(
											navItem.link_label.raw
										)}
									</Link>
								</li>
							);
						})}
					</ul>
					<Link
						to={nav.nav_button_url.url}
						target={nav.nav_button_url.target}
						className="banner-button"
					>
						{nav.nav_button_text}
					</Link>
				</div>
				<button
					className="navbar-toggler collapsed"
					type="button"
					data-toggle="collapse"
					data-target="#navbarCollapse"
					aria-controls="navbarCollapse"
					aria-expanded="false"
					aria-label="Toggle navigation"
				>
					<span> </span>
					<span> </span>
					<span> </span>
				</button>
			</nav>
		</header>
	);
};

export const query = graphql`
	fragment HeaderQuery on PrismicNavigation {
		data {
			nav_logo {
				url
			}
			nav_button_text
			nav_button_url {
				type
				uid
				url
				target
			}
			prismic_title {
				raw
			}
			top_navigation {
				link {
					type
					uid
					url
				}
				link_label {
					raw
				}
			}
		}
	}
`;

export default Header;
