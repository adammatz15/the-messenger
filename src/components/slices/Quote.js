import React from "react";
import { RichText } from "prismic-reactjs";

const Quote = ({ slice }) => (
	<section className="quote content-section">
		<div className="container">
			<blockquote>{RichText.asText(slice.primary.quote.raw)}</blockquote>
		</div>
	</section>
);

export default Quote;
