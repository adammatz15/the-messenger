import React from "react";
import { RichText } from "prismic-reactjs";

const ImageContent = ({ slice }) => (
	<section className="highlight content-section container">
		<div className="highlight-left">
			<img
				src={slice.primary.image_and_content_image.url}
				alt={slice.primary.image_and_content_image.alt}
			/>
		</div>
		<div className="highlight-right">
			<RichText
				class="image-content-title"
				render={slice.primary.image_and_content_title.raw}
			/>
			<RichText
				class="image-content-text"
				render={slice.primary.image_and_content_text.raw}
			/>
		</div>
	</section>
);

export default ImageContent;
