import React from "react";
import { Link } from "gatsby";
import { RichText } from "prismic-reactjs";

const ServiceMenu = ({ slice }) => {
	return (
		<section
			className="service-menu-section content-section bg-img"
			style={{
				backgroundImage: `url(${slice.primary.service_menu_background_image.url})`,
				backgroundSize: "cover",
			}}
		>
			<div className="container">
				<div className="row">
					<div className="col-xs-12">
						<div className="service-menu-title">
							{slice.primary.service_menu_title}
						</div>
					</div>
				</div>
				<div className="service-menu">
					<div className="row">
						{slice.items.map((serviceMenu, index) => (
							<a
								href={serviceMenu.service_menu_haircut_url.url}
								target={
									serviceMenu.service_menu_haircut_url.target
								}
								className=" "
								className="col-md service-menu-card service-menu-card"
								key={`service-menu=${index}`}
							>
								<div className="row">
									<div className="col text-center">
										<img
											src={
												serviceMenu
													.service_menu_teir_image.url
											}
										/>
									</div>
								</div>
								{serviceMenu.service_menu_haircut_one_title &&
								serviceMenu.service_menu_haircut_one_title ? (
									<div>
										<div className="row">
											<div className="col-8 service-menu-card-haircut-title">
												{
													serviceMenu.service_menu_haircut_one_title
												}
											</div>
											<div className="col-4 service-menu-card-haircut-price">
												{
													serviceMenu.service_menu_haircut_one_price
												}
											</div>
										</div>
										<p className="service-menu-card-haircut-description">
											{
												serviceMenu.service_menu_haircut_one_description
											}
										</p>
									</div>
								) : null}
								{serviceMenu.service_menu_haircut_two_title &&
								serviceMenu.service_menu_haircut_two_title ? (
									<div>
										<div className="row">
											<div className="col-8 service-menu-card-haircut-title">
												{
													serviceMenu.service_menu_haircut_two_title
												}
											</div>
											<div className="col-4 service-menu-card-haircut-price">
												{
													serviceMenu.service_menu_haircut_two_price
												}
											</div>
										</div>
										<p className="service-menu-card-haircut-description">
											{
												serviceMenu.service_menu_haircut_two_description
											}
										</p>
									</div>
								) : null}
								{serviceMenu.service_menu_haircut_three_title &&
								serviceMenu.service_menu_haircut_three_title ? (
									<div>
										<div className="row">
											<div className="col-8 service-menu-card-haircut-title">
												{
													serviceMenu.service_menu_haircut_three_title
												}
											</div>
											<div className="col-4 service-menu-card-haircut-price">
												{
													serviceMenu.service_menu_haircut_three_price
												}
											</div>
										</div>
										<p className="service-menu-card-haircut-description">
											{
												serviceMenu.service_menu_haircut_three_description
											}
										</p>
									</div>
								) : null}
							</a>
						))}
					</div>
				</div>
			</div>
		</section>
	);
};

export default ServiceMenu;
