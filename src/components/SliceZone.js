import React from "react";
import ServiceMenu from "./slices/ServiceMenu";
import FullWidthImage from "./slices/FullWidthImage";
import ImageGallery from "./slices/ImageGallery";
import ImageHighlight from "./slices/ImageHighlight";
import Quote from "./slices/Quote";
import Text from "./slices/Text";
import ImageContent from "./slices/ImageContent";

const SliceZone = ({ sliceZone }) => {
	const sliceComponents = {
		service_menu: ServiceMenu,
		full_width_image: FullWidthImage,
		image_gallery: ImageGallery,
		image_highlight: ImageHighlight,
		quote: Quote,
		text: Text,
		image___content: ImageContent,
	};

	const sliceZoneContent = sliceZone.map((slice, index) => {
		const SliceComponent = sliceComponents[slice.slice_type];
		if (SliceComponent) {
			return <SliceComponent slice={slice} key={`slice-${index}`} />;
		}
		return null;
	});

	return <section>{sliceZoneContent}</section>;
};

export default SliceZone;
