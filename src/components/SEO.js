import React from "react";
import { Helmet } from "react-helmet";
import { StaticQuery, graphql } from "gatsby";

const SEO = ({ title, description }) => (
	<StaticQuery
		query={`${SeoQuery}`}
		render={(data) => {
			const metaTitle = title
				? `${title} | ${data.site.siteMetadata.title}`
				: data.site.siteMetadata.title;
			const metaDescription =
				description || data.site.siteMetadata.description;

			return (
				<Helmet>
					<title>{metaTitle}</title>
					<meta name="description" content={metaDescription} />
					<script
						src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
						integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
						crossorigin="anonymous"
					></script>
					<script
						src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"
						integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns"
						crossorigin="anonymous"
					></script>
					<style>
						@import
						url('https://fonts.googleapis.com/css2?family=Montserrat&display=swap');
					</style>
				</Helmet>
			);
		}}
	/>
);

const SeoQuery = graphql`
	query {
		site {
			siteMetadata {
				title
				description
			}
		}
	}
`;

export default SEO;
