import React from "react";
import { graphql } from "gatsby";
import Layout from "../components/Layout";
import SEO from "../components/SEO";
import SliceZone from "../components/SliceZone";

const Page = ({ data }) => {
	if (!data) return null;
	const document = data.allPrismicPage.edges[0].node;
	const meta = data.allPrismicPage.edges[0].node.data;
	const prismicNavigation = data.prismicNavigation;

	const capitalizeFirstLetter = (input) => {
		return input[0].toUpperCase() + input.slice(1);
	};

	return (
		<Layout navigation={prismicNavigation}>
			<SEO
				title={capitalizeFirstLetter(meta.meta_title)}
				description={capitalizeFirstLetter(meta.meta_description)}
			/>
			<SliceZone sliceZone={document.data.body} />
		</Layout>
	);
};

export const query = graphql`
	query PageQuery($uid: String) {
		allPrismicPage(filter: { uid: { eq: $uid } }) {
			edges {
				node {
					uid
					data {
						meta_title
						meta_description
						body {
							... on PrismicPageBodyServiceMenu {
								id
								slice_type
								items {
									service_menu_haircut_one_title
									service_menu_haircut_one_price
									service_menu_haircut_one_description
									service_menu_haircut_two_title
									service_menu_haircut_two_price
									service_menu_haircut_two_description
									service_menu_haircut_three_title
									service_menu_haircut_three_price
									service_menu_haircut_three_description
									service_menu_haircut_url {
										url
										target
									}
									service_menu_teir_image {
										url
									}
								}
							}
							... on PrismicPageBodyText {
								slice_type
								primary {
									columns
									content {
										raw
									}
								}
							}
							... on PrismicPageBodyQuote {
								slice_type
								primary {
									quote {
										raw
									}
								}
							}
							... on PrismicPageBodyFullWidthImage {
								slice_type
								primary {
									full_width_image {
										url
										thumbnails
									}
								}
							}
							... on PrismicPageBodyImageGallery {
								slice_type
								primary {
									gallery_title {
										raw
									}
								}
								items {
									image {
										url
										thumbnails
										alt
									}
									image_description {
										raw
									}
									link {
										url
										type
										uid
									}
									link_label {
										raw
									}
								}
							}
							... on PrismicPageBodyImageHighlight {
								slice_type
								primary {
									featured_image {
										url
										thumbnails
										alt
									}
									title {
										raw
									}
									description {
										raw
									}
									link {
										url
										type
										uid
									}
									link_label {
										raw
									}
								}
							}
						}
					}
				}
			}
		}
		prismicNavigation {
			...HeaderQuery
		}
	}
`;

export default Page;
