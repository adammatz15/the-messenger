import React from "react";
import { graphql } from "gatsby";
import Layout from "../components/Layout";
import SEO from "../components/SEO";
import HomepageBanner from "../components/HomepageBanner";
import SliceZone from "../components/SliceZone";

const Homepage = ({ data }) => {
	if (!data) return null;
	const document = data.allPrismicHomepage.edges[0].node.data;

	const bannerContent = {
		title1: document.banner_title_1,
		title2: document.banner_title_2,
		description: document.banner_description,
		link: document.banner_link,
		linkLabel: document.banner_link_label,
		background: document.banner_background,
	};

	const prismicNavigation = data.prismicNavigation;
	console.log(document.body);

	return (
		<Layout isHomepage navigation={prismicNavigation}>
			<SEO title="Home" description="This is the homepage." />
			<HomepageBanner bannerContent={bannerContent} />
			<SliceZone sliceZone={document.body} />
			<iframe
				src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d12348.783403399195!2d-94.66283836582507!3d38.96615295721291!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xc578d7c26434f328!2sThe%20Messenger!5e0!3m2!1sen!2sus!4v1637991463836!5m2!1sen!2sus"
				width="1920"
				height="600"
				allowfullscreen=""
				loading="lazy"
			></iframe>
		</Layout>
	);
};

export const query = graphql`
	query Homepage {
		allPrismicHomepage {
			edges {
				node {
					data {
						banner_title_1
						banner_title_2
						banner_description {
							raw
						}
						banner_link {
							url
							type
							uid
						}
						banner_link_label {
							raw
						}
						banner_background {
							url
							thumbnails
							alt
						}
						body {
							... on PrismicHomepageBodyText {
								slice_type
								primary {
									columns
									content {
										raw
									}
								}
							}
							... on PrismicHomepageBodyQuote {
								slice_type
								primary {
									quote {
										raw
									}
								}
							}
							... on PrismicHomepageBodyFullWidthImage {
								slice_type
								primary {
									full_width_image {
										url
										thumbnails
									}
								}
							}
							... on PrismicHomepageBodyImageGallery {
								slice_type
								primary {
									gallery_title {
										raw
									}
								}
								items {
									image {
										url
										thumbnails
										alt
									}
									image_description {
										raw
									}
									link {
										url
										type
										uid
									}
									link_label {
										raw
									}
								}
							}
							... on PrismicHomepageBodyImageHighlight {
								slice_type
								primary {
									featured_image {
										url
										thumbnails
										alt
									}
									title {
										raw
									}
									description {
										raw
									}
									link {
										url
										type
										uid
									}
									link_label {
										raw
									}
								}
							}
							... on PrismicHomepageBodyImageContent {
								slice_type
								primary {
									image_and_content_title {
										raw
									}
									image_and_content_image {
										alt
										url
									}
									image_and_content_text {
										raw
									}
								}
							}
							... on PrismicHomepageBodyServiceMenu {
								slice_type
								id
								primary {
									service_menu_title
									service_menu_background_image {
										url
									}
								}
								items {
									service_menu_haircut_one_description
									service_menu_haircut_one_price
									service_menu_haircut_one_title
									service_menu_haircut_three_description
									service_menu_haircut_three_price
									service_menu_haircut_three_title
									service_menu_haircut_two_description
									service_menu_haircut_two_price
									service_menu_haircut_two_title
									service_menu_haircut_url {
										url
										target
									}
									service_menu_teir_image {
										url
									}
								}
							}
						}
					}
				}
			}
		}
		prismicNavigation {
			...HeaderQuery
		}
	}
`;

export default Homepage;
